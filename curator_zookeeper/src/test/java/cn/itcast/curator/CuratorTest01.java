package cn.itcast.curator;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.BackgroundCallback;
import org.apache.curator.framework.api.CuratorEvent;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;


public class CuratorTest01 {

    private CuratorFramework client;

    @Before
    public void connectionTest() {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(3000, 10);


        client = CuratorFrameworkFactory.newClient("192.168.23.129:2181",
                60000, 15000, retryPolicy);
       /* CuratorFramework client = CuratorFrameworkFactory.newClient("192.168.149.135:2181",
                60 * 1000, 15 * 1000, retryPolicy);*/

        client.start();
    }


    //=========================create=======================================================
    @Test
    public void createTest() throws Exception {

        /**
         * 创建节点：create 持久 临时 顺序 数据
         * 1. 基本创建 ：create().forPath("")
         * 2. 创建节点 带有数据:create().forPath("",data)
         * 3. 设置节点的类型：create().withMode().forPath("",data)
         * 4. 创建多级节点  /app1/p1 ：create().creatingParentsIfNeeded().forPath("",data)
         */

        //2. 创建节点 带有数据:create().forPath("",data)
        String path = client.create().forPath("/app2", "wocao".getBytes());
        System.out.println(path);

      /*  //3. 设置节点的类型：create().withMode().forPath("",data)
        String path1 = client.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT_SEQUENTIAL).forPath("/app1/p1", "hhee".getBytes());
        System.out.println(path1);*/
    }

    //=========================get=======================================================

    /**
     * 查询节点：
     * 1. 查询数据：get: getData().forPath()
     * 2. 查询子节点： ls: getChildren().forPath()
     * 3. 查询节点状态信息：ls -s:getData().storingStatIn(状态对象).forPath()
     */
    @Test
    public void getTest() throws Exception {
        //1. 查询数据：get: getData().forPath()
        /*byte[] bytes = client.getData().forPath("/app1");
        System.out.println(new String(bytes));*/

        // 2. 查询子节点： ls: getChildren().forPath()
       /* List<String> list = client.getChildren().forPath("/app1");
        System.out.println(list);*/


        //3. 查询节点状态信息：ls -s:getData().storingStatIn(状态对象).forPath()
        //创建 状态的对象
        Stat stat = new Stat();
        client.getData().storingStatIn(stat).forPath("/app1");
        System.out.println(stat);
    }


    //===========================set================================================================================

    /**
     * 修改数据
     * 1. 基本修改数据：setData().forPath()
     * 2. 根据版本修改: setData().withVersion().forPath()
     * * version 是通过查询出来的。目的就是为了让其他客户端或者线程不干扰我。
     *
     * @throws Exception
     */
    @Test
    public void setTest() throws Exception {
        //1. 基本修改数据：setData().forPath()
        /*client.setData().forPath("/app1/p2","aaaa".getBytes());*/

        //2. 根据版本修改: setData().withVersion().forPath()
        //获取当前数据的版本信息
        Stat stat = new Stat();
        client.getData().storingStatIn(stat).forPath("/app1/p2");
        client.setData().withVersion(stat.getVersion()).forPath("/app1/p2", "bbbbbb".getBytes());
    }


    //===========================delete================================================================================

    /**
     * 删除节点： delete deleteall
     * 1. 删除单个节点:delete().forPath("/app1");
     * 2. 删除带有子节点的节点:delete().deletingChildrenIfNeeded().forPath("/app1");
     * 3. 必须成功的删除:为了防止网络抖动。本质就是重试。  client.delete().guaranteed().forPath("/app2");
     * 4. 回调：inBackground
     *
     * @throws Exception
     */
    @Test
    public void deleteTest() throws Exception {
        //1. 删除单个节点:delete().forPath("路径");
        /* client.delete().forPath("/app3");*/

        //2. 删除带有子节点的节点:delete().deletingChildrenIfNeeded().forPath("/app1");
        //guaranteed() 强制删除
       /* client.delete().guaranteed().deletingChildrenIfNeeded().forPath("/app1");*/

        //回调：inBackground
        client.delete().guaranteed().inBackground(new BackgroundCallback() {
            @Override
            public void processResult(CuratorFramework curatorFramework, CuratorEvent curatorEvent) throws Exception {
                System.out.println("我被删除了");
                System.out.println(curatorEvent);
            }
        }).forPath("/app2");
    }

    @After
    public void close() {
        if (client != null) {
            client.close();
        }
    }

}
